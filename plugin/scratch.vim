function Newscratch()
  execute 'tabnew '
  file *scratch*
  setlocal buftype=nofile
  setlocal bufhidden=hide
  setlocal noswapfile readonly
endfunction

command! -nargs=0 Scratch call Newscratch()
