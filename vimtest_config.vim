nmap <silent> <leader>Tn :TestNearest<CR>
nmap <silent> <leader>Tf :TestFile --no-color<CR>
nmap <silent> <leader>S :TestFile -strategy=neoterm<CR>
nmap <silent> <leader>tt :TestNearest -strategy=dispatch --no-color --format failures<CR>
nmap <silent> <leader>Tl :TestLast<CR>
nmap <silent> <leader>Tv :TestVisit<CR>

let test#strategy = {
  \ 'nearest': 'neoterm',
  \ 'file':    'dispatch',
  \ 'suite':   'dispatch',
\}

" for neovim
let test#neovim#term_position = "belowright"

" au FileType ruby map <Leader>t :call RunCurrentSpecFile()<CR>
nmap <Leader>t :TestFile --no-color --format failures<CR>
" au FileType ruby map <Leader>s :call RunNearestSpec()<CR>
nmap <Leader>s :TestNearest --format documentation<CR>
" au FileType ruby map <Leader>l :call RunLastSpec()<CR>
nmap <Leader>l :TestLast<CR>
" au FileType ruby map <Leader>a :call RunAllSpecs()<CR>
"
let g:test#ruby#rspec#executable = 'rspec'

function! SpringTransform(cmd) abort
  if filereadable("./bin/spring")
    return './bin/spring ' . a:cmd
  else
    return a:cmd
  end
endfunction

let g:test#custom_transformations = {'ruby#rspec': function('SpringTransform')}
let g:test#transformation = 'ruby#rspec'

let g:dispatch_compilers = {}
let g:dispatch_compilers['./bin/spring'] = ''

