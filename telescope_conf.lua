local telescope = require('telescope')
local builtins = require('telescope.builtin')

telescope.setup {
  -- opts...
}
telescope.load_extension('hoogle')

vim.keymap.set('n', 'ff', builtins.find_files)
vim.keymap.set('n', 'fg', builtins.live_grep)
vim.keymap.set('n', 'fb', builtins.buffers)
vim.keymap.set('n', 'fh', builtins.help_tags)
