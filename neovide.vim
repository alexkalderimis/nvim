if exists('g:neovide')
  set guifont=Fira\ Code:h14

  nnoremap <A-v> "+p
  inoremap <A-v> <C-o>"+p<CR>
  cnoremap <A-v> <c-r>+

  let g:neovide_cursor_animation_length=0.03

  set background=dark
  colorscheme nord
  set cursorline
endif
