" ALE Configuration
let g:ale_linters_explicit = 1
let g:ale_lint_on_enter = 1
let g:ale_lint_on_text_changed = 1
let g:ale_fix_on_save = 1
let g:ale_ruby_rubocop_executable = 'bundle'
let g:ale_change_sign_column_color = 1
let g:ale_cursor_detail = 0
let g:ale_echo_delay = 250
let g:airline#extensions#ale#enabled = 1

set previewheight=3

let g:ale_linters ={
      \   'haskell': ['hlint'],
      \   'go': ['gofmt', 'golint', 'gopls'],
      \   'markdown': ['md_lint'],
      \   'ruby': ['ruby', 'rubocop']
      \}
let g:ale_fixers = {
      \   'javascript': ['prettier'],
      \   'typescript': ['tsserver', 'tslint'],
      \   'css': ['prettier'],
      \   'vue': ['prettier'],
      \   'go': ['gofmt'],
      \   'ruby': ['rubocop'],
      \   'haskell': ['brittany', 'hlint']
      \}


command ShowHint call ale#cursor#ShowCursorDetail()

let g:ale_completion_tsserver_autoimport = 1

nnoremap <leader>at :ALEHover<CR>
nnoremap <leader>al :ALELint<CR>
nnoremap <leader>] :ALEGoToDefinition<CR>
nnoremap <leader>aj :ALENext<CR>
