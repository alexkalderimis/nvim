:call deoplete#custom#var('gitlab-users', 'project_path', 'gitlab-org/gitlab')
:call deoplete#custom#var('gitlab-labels', 'project_path', 'gitlab-org/gitlab')
:call deoplete#custom#var('gitlab-merge-requests', 'project_path', 'gitlab-org/gitlab')
:GhostStart 
